import 'dart:async';

import 'package:blueotter/components/count_down_widget.dart';
import 'package:blueotter/components/time_setting_widget.dart';
import 'package:blueotter/shared/ui_colors.dart';
import 'package:blueotter/shared/ui_dimens.dart';
import 'package:blueotter/view/setting_page.dart';
import 'package:blueotter/viewmodels/count_down_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CountDownPage extends StatefulWidget {
  const CountDownPage({Key? key}) : super(key: key);

  @override
  _CountDownPageState createState() => _CountDownPageState();
}

class _CountDownPageState extends State<CountDownPage>
    with SingleTickerProviderStateMixin {
  late CountDownViewModel _countDownViewModel;
  late AnimationController _controller;
  late Animation _animation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 3),
    );

    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _countDownViewModel.start();
      }
    });

    _animation = Tween(begin: 3.0, end: 0.0).animate(_controller);

    _countDownViewModel = CountDownViewModel()..onInit();
  }

  @override
  void dispose() {
    _countDownViewModel.dispose();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _countDownViewModel,
      builder: (BuildContext ctx, _) {
        return Scaffold(
          body: Container(
            padding: EdgeInsets.only(
              top: DimensManager.dimens.statusBarHeight,
              bottom: DimensManager.dimens.indicatorBarHeight,
            ),
            height: DimensManager.dimens.fullHeight,
            width: DimensManager.dimens.fullWidth,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildNavigationHeader(ctx),
                _buildBody(ctx),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody(BuildContext ctx) {
    final countdownTime =
        ctx.select((CountDownViewModel vm) => vm.countdownTime);
    final remainingTime =
        ctx.select((CountDownViewModel vm) => vm.remainingTime);
    final isCounting = ctx.select((CountDownViewModel vm) => vm.isCounting);
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Stack(
            children: [
              SizedBox(
                child: CountDownWidget(
                  counter: remainingTime,
                  percent: remainingTime / countdownTime,
                  size: DimensManager.dimens.setRadius(200),
                ),
                height: DimensManager.dimens.setRadius(250),
                width: DimensManager.dimens.setRadius(270),
              ),
              Positioned(
                child: TimeSettingWidget(time: countdownTime),
                right: 0,
              ),
            ],
          ),
          ElevatedButton(
            onPressed: () {
              if (_countDownViewModel.isCounting) {
                _countDownViewModel.stop();
              } else {
                showLoading();
              }
            },
            style: ElevatedButton.styleFrom(
              primary: UIColors.backgroundCountDown,
              elevation: 0,
              side: const BorderSide(color: UIColors.line),
              padding: EdgeInsets.symmetric(
                vertical: DimensManager.dimens.setHeight(12),
                horizontal: DimensManager.dimens.setWidth(70),
              ),
              shadowColor: Colors.transparent,
            ),
            child: Text(
              isCounting ? 'STOP' : 'START',
              style: TextStyle(
                color: UIColors.text,
                fontSize: DimensManager.dimens.setSp(20),
                fontWeight: FontWeight.w600,
                height: 1,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildNavigationHeader(BuildContext ctx) {
    final countdownTime =
        ctx.select((CountDownViewModel vm) => vm.countdownTime);
    return SizedBox(
      width: DimensManager.dimens.fullWidth,
      height: DimensManager.dimens.headerHeight,
      child: Row(
        children: [
          const Spacer(),
          IconButton(
            onPressed: () async {
              final time = await Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => SettingPage(
                    countdownSetting: countdownTime,
                  ),
                ),
              );

              if (time == null) return;

              _countDownViewModel.updateTime(time);
            },
            icon: Icon(
              CupertinoIcons.settings,
              size: DimensManager.dimens.setRadius(36),
            ),
            iconSize: DimensManager.dimens.setRadius(40),
          ),
        ],
      ),
    );
  }

  void showLoading() {
    OverlayState? overlayState = Overlay.of(context);
    _controller.reset();
    _controller.forward();
    OverlayEntry entry = OverlayEntry(builder: (context) {
      return Material(
        color: Colors.black.withOpacity(0.9),
        child: Center(
          child: SizedBox(
            child: AnimatedBuilder(
              animation: _animation,
              builder: (_, __) {
                return CountDownWidget(
                  counter: _animation.value.ceil(),
                  percent: _animation.value / 3,
                  size: DimensManager.dimens.setRadius(200),
                );
              },
            ),
            height: DimensManager.dimens.setRadius(250),
            width: DimensManager.dimens.setRadius(270),
          ),
        ),
      );
    });

    overlayState?.insert(entry);

    Timer(const Duration(seconds: 3), () {
      entry.remove();
    });
  }
}
