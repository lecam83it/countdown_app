import 'package:blueotter/components/time_setting_widget.dart';
import 'package:blueotter/shared/ui_colors.dart';
import 'package:blueotter/shared/ui_dimens.dart';
import 'package:blueotter/viewmodels/setting_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingPage extends StatefulWidget {
  final int countdownSetting;

  const SettingPage({
    Key? key,
    required this.countdownSetting,
  }) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  late SettingViewModel _settingViewModel;
  late FocusNode _customFocusNode;
  late TextEditingController _editingController;

  @override
  void initState() {
    super.initState();
    _settingViewModel = SettingViewModel();
    if (widget.countdownSetting == 30 || widget.countdownSetting == 45) {
      _editingController = TextEditingController();
    } else {
      _editingController = TextEditingController(
        text: widget.countdownSetting.toString(),
      );
    }

    _settingViewModel.onInit(time: widget.countdownSetting);

    _customFocusNode = FocusNode()
      ..addListener(() {
        _settingViewModel.onCustomFocus(_customFocusNode.hasFocus);
      });
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _settingViewModel,
      builder: (BuildContext ctx, _) {
        return Scaffold(
          backgroundColor: UIColors.background,
          body: GestureDetector(
            onTap: () {
              FocusManager.instance.primaryFocus?.unfocus();
            },
            child: Container(
              padding: EdgeInsets.only(
                top: DimensManager.dimens.statusBarHeight,
                bottom: DimensManager.dimens.indicatorBarHeight,
              ),
              height: DimensManager.dimens.fullHeight,
              width: DimensManager.dimens.fullWidth,
              child: Column(
                children: [
                  _buildNavigationHeader(),
                  _buildBody(ctx),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildNavigationHeader() {
    return SizedBox(
      width: DimensManager.dimens.fullWidth,
      height: DimensManager.dimens.headerHeight,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(
              CupertinoIcons.back,
              size: DimensManager.dimens.setRadius(36),
            ),
            iconSize: DimensManager.dimens.setRadius(40),
          ),
          Text(
            "BACK",
            style: TextStyle(
              fontSize: DimensManager.dimens.setSp(24),
              fontWeight: FontWeight.w600,
              color: UIColors.text,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBody(BuildContext ctx) {
    final settingTime = ctx.select((SettingViewModel vm) => vm.settingTime);
    final isConfigTime = ctx.select((SettingViewModel vm) => vm.isConfigTime);
    return Expanded(
      child: Container(
        color: UIColors.background,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TimeSettingWidget(
                  onPressed: () {
                    _editingController.text = "";
                    FocusManager.instance.primaryFocus?.unfocus();
                    ctx.read<SettingViewModel>().onChangeSettingTime(30);
                  },
                  time: 30,
                  timeStyle: TextStyle(
                    fontSize: DimensManager.dimens.setSp(48),
                    fontWeight: FontWeight.w700,
                  ),
                  unitStyle: TextStyle(
                    fontSize: DimensManager.dimens.setSp(30),
                  ),
                  isVisible: settingTime == 30,
                  isActive: isConfigTime,
                ),
                TimeSettingWidget(
                  onPressed: () {
                    _editingController.text = "";
                    FocusManager.instance.primaryFocus?.unfocus();
                    ctx.read<SettingViewModel>().onChangeSettingTime(45);
                  },
                  time: 45,
                  timeStyle: TextStyle(
                    fontSize: DimensManager.dimens.setSp(50),
                    fontWeight: FontWeight.w700,
                  ),
                  unitStyle: TextStyle(
                    fontSize: DimensManager.dimens.setSp(30),
                  ),
                  isVisible: settingTime == 45,
                  isActive: isConfigTime,
                )
              ],
            ),
            SizedBox(height: DimensManager.dimens.setHeight(48)),
            SizedBox(
              width: DimensManager.dimens.fullWidth / 2,
              child: TextField(
                focusNode: _customFocusNode,
                controller: _editingController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(
                    vertical: DimensManager.dimens.setHeight(8),
                    horizontal: DimensManager.dimens.setWidth(16),
                  ),
                  border: OutlineInputBorder(
                    borderSide: const BorderSide(color: UIColors.line),
                    borderRadius: BorderRadius.circular(50.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: UIColors.line),
                    borderRadius: BorderRadius.circular(50.0),
                  ),
                  hintStyle: TextStyle(
                    color: UIColors.placeHolder,
                    fontSize: DimensManager.dimens.setSp(24),
                    fontWeight: FontWeight.w600,
                  ),
                  hintText: "CUSTOM",
                ),
                cursorColor: UIColors.line,
                keyboardType: TextInputType.number,
                textAlignVertical: TextAlignVertical.center,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: UIColors.text,
                  fontSize: DimensManager.dimens.setSp(24),
                  fontWeight: FontWeight.w600,
                ),
                onChanged: (value) {
                  ctx.read<SettingViewModel>().onCustomChangeSettingTime(value);
                },
              ),
            ),
            SizedBox(height: DimensManager.dimens.setHeight(48)),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop(_settingViewModel.time);
              },
              style: ElevatedButton.styleFrom(
                primary: UIColors.backgroundCountDown,
                elevation: 0,
                side: const BorderSide(color: UIColors.line),
                padding: EdgeInsets.symmetric(
                  vertical: DimensManager.dimens.setHeight(12),
                  horizontal: DimensManager.dimens.setWidth(70),
                ),
                shadowColor: Colors.transparent,
              ),
              child: Text(
                'SAVE',
                style: TextStyle(
                  color: UIColors.text,
                  fontSize: DimensManager.dimens.setSp(20),
                  fontWeight: FontWeight.w600,
                  height: 1,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
