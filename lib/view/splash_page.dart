import 'dart:async';

import 'package:blueotter/shared/ui_colors.dart';
import 'package:blueotter/shared/ui_dimens.dart';
import 'package:blueotter/view/countdown_page.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    Timer(const Duration(seconds: 2), () {
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => const CountDownPage()),
        (route) => false,
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UIColors.background,
      body: SizedBox(
        height: DimensManager.dimens.fullHeight,
        width: DimensManager.dimens.fullWidth,
        child: Image.asset(
          "assets/img/dab_counter.png",
          width: DimensManager.dimens.setWidth(200),
          height: DimensManager.dimens.setHeight(150),
        ),
      ),
    );
  }
}
