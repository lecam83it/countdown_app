import 'package:blueotter/shared/ui_dimens.dart';
import 'package:flutter/material.dart';

class TriangleWidget extends StatelessWidget {
  final Color color;

  const TriangleWidget({
    Key? key,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: DimensManager.dimens.setRadius(30),
      width: DimensManager.dimens.setRadius(30),
      child: CustomPaint(painter: TrianglePainter(color: color)),
    );
  }
}

class TrianglePainter extends CustomPainter {
  final Color color;

  TrianglePainter({required this.color});

  @override
  void paint(Canvas canvas, Size size) {
    Paint painter = Paint()
      ..color = color
      ..strokeWidth = 1
      ..style = PaintingStyle.fill;

    var path = Path();

    path.moveTo(size.width / 2, 0);
    path.lineTo(size.height, size.width);
    path.lineTo(0, size.height);
    path.lineTo(size.width / 2, 0);
    path.close();

    canvas.drawPath(path, painter);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
