import 'dart:math';

import 'package:blueotter/shared/ui_colors.dart';
import 'package:blueotter/shared/ui_dimens.dart';
import 'package:flutter/material.dart';

class CountDownWidget extends StatelessWidget {
  final double size;
  final int counter;
  final double percent;

  const CountDownWidget({
    Key? key,
    required this.counter,
    required this.percent,
    required this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          height: size,
          width: size,
          decoration: BoxDecoration(
            color: UIColors.backgroundCountDown,
            borderRadius: BorderRadius.circular(size * 0.8),
          ),
          child: Center(
            child: Text(
              counter.toString(),
              style: TextStyle(
                height: 1,
                fontSize: DimensManager.dimens.setSp(48),
                color: UIColors.text,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        SizedBox(
          height: size,
          width: size,
          child: CustomPaint(
            painter: CircularIndicator(
              percent: percent,
            ),
          ),
        ),
      ],
    );
  }
}

class CircularIndicator extends CustomPainter {
  final double percent;

  CircularIndicator({
    required this.percent,
  });

  @override
  void paint(Canvas canvas, Size size) {
    Offset center = Offset(size.width / 2, size.height / 2);
    double radius = min(size.width / 2, size.height / 2);
    Rect circle = Rect.fromCircle(center: center, radius: radius);
    const double maxAngle = 1.0;
    double angle = 2 * pi * maxAngle;

    Paint outerPaint = Paint()
      ..strokeWidth = size.width * 0.04
      ..color = UIColors.border
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    canvas.drawArc(circle, pi / 2, angle, false, outerPaint);

    Paint progressPaint = Paint()
      ..strokeWidth = size.width * 0.04
      ..color = UIColors.line
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    canvas.drawArc(circle, -pi / 2, angle * percent, false, progressPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
