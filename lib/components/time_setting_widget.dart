import 'package:blueotter/components/triangle_widget.dart';
import 'package:blueotter/shared/ui_colors.dart';
import 'package:blueotter/shared/ui_dimens.dart';
import 'package:flutter/material.dart';

class TimeSettingWidget extends StatelessWidget {
  final int time;
  final TextStyle? timeStyle;
  final TextStyle? unitStyle;
  final bool isVisible;
  final bool isActive;
  final VoidCallback? onPressed;

  const TimeSettingWidget({
    Key? key,
    required this.time,
    this.timeStyle,
    this.unitStyle,
    this.isVisible = false,
    this.isActive = false,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                time.toString(),
                style: TextStyle(
                  height: 1,
                  fontSize: DimensManager.dimens.setSp(36),
                  color: (isVisible && isActive)
                      ? UIColors.text
                      : UIColors.placeHolder,
                  fontWeight: FontWeight.w600,
                ).merge(timeStyle),
              ),
              Text(
                's',
                style: TextStyle(
                  height: 1,
                  fontSize: DimensManager.dimens.setSp(20),
                  color: (isVisible && isActive)
                      ? UIColors.text
                      : UIColors.placeHolder,
                  fontWeight: FontWeight.w600,
                ).merge(unitStyle),
              ),
            ],
          ),
          if (isVisible)
            TriangleWidget(
              color: (isVisible && isActive)
                  ? UIColors.text
                  : UIColors.placeHolder,
            ),
          if (!isVisible)
            SizedBox(
              height: DimensManager.dimens.setRadius(30),
              width: DimensManager.dimens.setRadius(30),
            ),
        ],
      ),
    );
  }
}
