import 'package:flutter/foundation.dart';

class SettingViewModel extends ChangeNotifier {
  int _settingTime = 30; // seconds
  int get settingTime => _settingTime;

  int _customSettingTime = -1;

  bool _customFocus = false; // seconds

  bool get isConfigTime =>
      (_settingTime == 30 || _settingTime == 45) &&
      !_customFocus &&
      _customSettingTime == -1;

  int get time => (_customSettingTime != -1) ? _customSettingTime : _settingTime;

  void onInit({required int time}) {
    if(time == 30 || time == 45) {
      _settingTime = time;
      notifyListeners();
      return;
    }
    _customSettingTime = time;
    notifyListeners();
  }

  void onChangeSettingTime(int value) {
    _settingTime = value;
    _customSettingTime = -1;
    notifyListeners();
  }

  void onCustomChangeSettingTime(String value) {
    if (value.trim() == "") {
      _customSettingTime = -1;
      notifyListeners();
      return;
    }
    _customSettingTime = int.parse(value);
    notifyListeners();
  }

  void onCustomFocus(bool hasFocus) {
    _customFocus = hasFocus;
    notifyListeners();
  }
}
