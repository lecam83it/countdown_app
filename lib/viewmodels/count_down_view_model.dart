import 'dart:async';

import 'package:flutter/material.dart';

class CountDownViewModel extends ChangeNotifier {
  void onInit() {
    _remainingTime = _countdownTime;
  }

  int _countdownTime = 30;

  int get countdownTime => _countdownTime;

  void updateTime(int time) {
    _countdownTime = time;
    _remainingTime = _countdownTime;
    notifyListeners();
  }

  int _remainingTime = 0;

  int get remainingTime => _remainingTime;

  bool _isCounting = false;

  bool get isCounting => _isCounting;
  Timer? _timer;

  void start() {
    if (_isCounting) return;
    _isCounting = true;
    notifyListeners();

    int _direction = -1;
    const oneSecond = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSecond,
      (Timer timer) {
        if (_remainingTime == 0) {
          _direction = 1;
        }

        _remainingTime += 1 * _direction;

        if (_remainingTime == _countdownTime && _direction == 1) {
          _isCounting = false;
          timer.cancel();
        }

        notifyListeners();
      },
    );
  }

  void stop() {
    if (!_isCounting) return;
    _remainingTime = _countdownTime;
    _isCounting = false;
    _timer?.cancel();
    notifyListeners();
  }
}
